<?php

require "Hashids/HashGenerator.php";
require "Hashids/Hashids.php";

function prepare_meal_title($title){
    $title = str_replace('(adult/youth)', '', $title);
   $title = str_replace('(child)', 'C', $title);
   $title = strtoupper($title);
   return $title;
}

class CRM_Contact_Form_Task_Dymo extends CRM_Contact_Form_Task {
    function buildQuickForm()
    {

        CRM_Core_Resources::singleton()->addScriptFile('com.netasi.badge', 'jquery-min.js');
        CRM_Core_Resources::singleton()->addScriptFile('com.netasi.badge', 'underscore-min.js');
        CRM_Core_Resources::singleton()->addScriptFile('com.netasi.badge', 'some.js');
    	// extract user-entered newsletter badge
		$queryParams = $this->get('queryParams');
		//get ids from params
		$ids = array();
		foreach ($queryParams as $key => $value) {
			$name = $value[0];
			if (substr($name, 0,7) == 'mark_x_'){
				$ids[] = substr($name, 7);
			}
		}

		$ids_list = implode(', ', $ids);
		// befor query about all participants
		// get event name for slat
		$sql = "SELECT ev.title from civicrm_participant pt left join  civicrm_event ev on ev.id=pt.event_id where pt.id={$ids[0]} ";
		$dao = CRM_Core_DAO::executeQuery($sql);
		$dao->fetch();
		$salt = get_object_vars($dao)['title'];
  		// var_dump($salt);
		$hashids = new Hashids\Hashids($salt);

		// ok, now we need all titles from all lineitems that are among existed items
		$titles = array();
        /*************************
                 CONFIG 
        ***************************/
		// all values from database 
		$exhibitor_ids = array(434, 417, 385,386,387);
		$exibitor_assistant_ids = array(439,408,392);
		$child_7meal = 381;
		$adult_7meal = 373;
        $breakfast_items = array($adult_7meal, 366 ); //???
        $breakfast_items = array_merge($breakfast_items, $exhibitor_ids);

		// first time attendee ??
		$child_meal_ids = array (374,375,376,377,378,379,380);
		$adult_meal_ids = array (366,367,368,369,370,371,372);

        // for statuses
        $kinder_ids = array(428,411,388);
        $primary_ids = array(429,412,389);
        $junior_ids = array(430,413,390);
        $earliteen_ids = array(431,414,391);
        $youth_ids = array(432,415,383);

		$young_ids = array_merge($kinder_ids, $primary_ids, $earliteen_ids, $junior_ids, $youth_ids);
        $singleday_ids = array(433,416,393);

        $wifi_id = 435;

        /************ END CONFIG ******************/

		$litems_list = $exibitor_assistant_ids;
        array_push($litems_list, $wifi_id);
		$litems_list = implode ( ',', array_merge($litems_list, $child_meal_ids, $adult_meal_ids, $young_ids, $exhibitor_ids));

		$sql  = "SELECT id,label from civicrm_price_field_value where id in ($litems_list)";
		$dao = CRM_Core_DAO::executeQuery($sql);
		while ($dao->fetch()) {
      		$row = get_object_vars($dao);
      		// encode particiapnt id to hashids
      		$titles[ $row['id'] ] = prepare_meal_title($row['label']);
    	}
	     		// var_dump($titles);


		// HERE you set ALL AVAILABLE OPTONS
		$sql = "SELECT pt.id, ov.label as prefix, ovv.label as suffix, cn.id as orig_id, cn.first_name, cn.last_name, ad.city, country.name as country, st.name as state, pt.registered_by_id, pt.status_id,
		    GROUP_CONCAT(li.price_field_value_id SEPARATOR ',')as lineitems
		    , pt.role_id as role
		    , fta.first_time_to_attend_asi_convent_48 as fta
            , fta.organization_name_49 as company
			FROM civicrm_participant pt 
			left join civicrm_contact    cn  on cn.id=pt.contact_id
            left outer join civicrm_option_value ov on (ov.option_group_id=6 and ov.value=cn.prefix_id)
            left outer join civicrm_option_value ovv on (ovv.option_group_id=7 and ovv.value=cn.suffix_id)
			left outer join civicrm_address ad on ad.contact_id=cn.id and is_primary='1'
			left outer join civicrm_country country on country.id = ad.country_id
			left outer join civicrm_state_province st on st.id = ad.state_province_id
			left outer join civicrm_line_item li on (li.entity_table = 'civicrm_participant' AND li.entity_id = pt.id AND li.qty <> 0)
            left outer join civicrm_value_first_time_attendee_6 fta on fta.entity_id=pt.id 
			 
			where pt.id in ($ids_list) 
			group by pt.id 
		";
		$dao = CRM_Core_DAO::executeQuery($sql);
		$all = array();
		while ($dao->fetch()) {
			$meals = array();
      		$one_row = get_object_vars($dao);

            // gathering ministry, city, state
            // if some is empty
                // var_dump('erf');
            if ( !empty($one_row['registered_by_id'])) {

                if ( empty($one_row['state']) || empty($one_row['city']) || empty($one_row['company'])) {
                    $sql2 = "SELECT country.name as country, ad.city, st.name as state, 
                    fta.organization_name_49 as company ,
                    cn.display_name as display_name
                    from civicrm_participant pt
                    left join civicrm_contact    cn  on cn.id=pt.contact_id
                    left outer join civicrm_address ad on ad.contact_id=cn.id and is_primary='1'
                    left outer join civicrm_state_province st on st.id = ad.state_province_id
                    left outer join civicrm_country country on country.id = ad.country_id
                    left outer join civicrm_value_first_time_attendee_6 fta on fta.entity_id=pt.id
                    where pt.id={$one_row['registered_by_id']}
                    ";
                    $dao2 = CRM_Core_DAO::executeQuery($sql2);
                    $dao2->fetch();
                    $dao2 = get_object_vars($dao2);

                    $one_row['display_name'] = $dao2['display_name'];
                    if ( empty($one_row['state']) ) {
                        $one_row['state_changed'] = true;
                        $one_row['state'] = $dao2['state'];
                    }
                    if ( empty($one_row['city']) ){
                        $one_row['city_changed'] = true;
                        $one_row['city'] = $dao2['city'];
                    }
                    if ( empty($one_row['country']) ){
                        $one_row['country_changed'] = true;
                        $one_row['country'] = $dao2['country'];
                    }
                    if ( empty($one_row['company']) ) {
                        $one_row['company_changed'] = true;
                        $one_row['company'] = $dao2['company'];
                    }

                }
            }
     		
     		// ok, now  we need to add all meals that user has
     		$user_items = split(',', $one_row['lineitems']);
     		// var_dump($user_items);
     		$a77 = array($child_7meal,$adult_7meal);
     		$tmp = array_intersect ( array_merge($exhibitor_ids, $a77 ), $user_items );
     		// if we need to print all meals
     		if ( !empty ($tmp) ) {
     			$ids_array = $adult_meal_ids;
     			if (in_array($child_7meal, $user_items) ) {
     				$ids_array = $child_meal_ids;
     			}

     			// print all meals
     			foreach ($ids_array as $key => $value) {
                    $meals[] = $titles[$value]; 
     			}
     		}
     		// CHANGE TO ELSE IN FUTURE
            // temporary change to else
            else {
                $all_meals = array_merge($adult_meal_ids, $child_meal_ids);
                $intersect = array_intersect($all_meals, $user_items);
                // var_dump($intersect);
                foreach ($intersect as $key => $value) {
                    $meals[] = $titles[$value];     
                } 
            }

     		if ( !empty ($meals) ) {
     			$one_row['meals'] = $meals;	
     		}



     		// next does we need to print wifi
     		$ex_plus_ids = array_merge($exhibitor_ids, $exibitor_assistant_ids);
     		$tmp = array_intersect($ex_plus_ids, $user_items);
     		$is_exibitor_man = ( !empty( $tmp  ) or ($one_row['role']==2) );
            if ($is_exibitor_man) $one_row['is_exibitor_man'] = "True";
            if (in_array($wifi_id, $user_items)){
     			$one_row['wifi'] = 'Exhibitor';
            }

     		// next we do group
     		$sql2 = "SELECT gr.title from civicrm_participant pt
     			left join civicrm_contact cn on cn.id = pt.contact_id 
     			left join civicrm_group_contact grc on grc.contact_id=cn.id 
				left join civicrm_group gr on grc.group_id = gr.id
     			where pt.id={$one_row['id']}
     		";
     		$dao2 = CRM_Core_DAO::executeQuery($sql2);
     		$groups = array();
     		while ( $dao2->fetch()) {
     			$res2 = get_object_vars($dao2);
     			$groups[] = $res2['title'];
     		}

     		$ggg1 = 'ASI Officer';
     		$ggg2 = 'ASI Board Member';
     		$ggg3 = 'ASI Staff';

 			$badge_type = 'Attendee'; 
     		if ( in_array( $ggg1, $groups) ) {
     			$badge_type = $ggg1;
     		} elseif ( in_array($ggg2, $groups)) {
     			$badge_type = $ggg2;
     		} elseif ( in_array($ggg3, $groups)) {
     			$badge_type = $ggg3;
     		} else {
     			// check if member
     			$is_member = false;
     			if (!empty($one_row['registered_by_id'])){ 
     				$cond = "(pt.id={$one_row['id']} or pt.id={$one_row['registered_by_id']} )";
     			}
     			else {
     				$cond = "pt.id={$one_row['id']}"; 
     			}
     			// $sql2 = " SELECT * from civicrm_participant pt 
     			// 	left join civicrm_contact cn on cn.id = pt.contact_id 
     			// 	left join civicrm_membership mm on mm.contact_id = cn.id
     			// 	where mm.end_date >= curdate() and $cond 
     			// ";
                $sql2 = " SELECT * from civicrm_participant pt 
                    left join civicrm_contact cn on cn.id = pt.contact_id 
                    left join civicrm_membership mm on mm.contact_id = cn.id
                    where (mm.status_id=1 or mm.status_id=2) and $cond 
                ";
     			$dao2 = CRM_Core_DAO::executeQuery($sql2);
     			if ( $dao2->fetch() ) {
     				$is_member = true;
     			}

                // check if he have new membership
                $sql2 = " SELECT * from civicrm_participant pt 
                    left join civicrm_contact cn on cn.id = pt.contact_id 
                    left join civicrm_membership mm on mm.contact_id = cn.id
                    where mm.join_date >=  DATE_SUB(NOW(),INTERVAL 1 YEAR) and pt.id={$one_row['id']}
                ";
                $dao2 = CRM_Core_DAO::executeQuery($sql2);
                if ( $dao2->fetch() ) {
                    $one_row['is_new'] = true;
                }
            // var_dump($one_row);

     			if ($is_member or $is_exibitor_man) {

	     			$part1 = $is_member ? 'ASI Member' : '';
	     			$part2 = $is_exibitor_man ? 'Exhibitor' : '';

	     			$badge_type = $part1.' '.$part2;
     			}
     			
     				// for juniors etc
                if ( count(array_intersect($kinder_ids, $user_items)) > 0) $badge_type = 'Kindergarten';
                if ( count(array_intersect($primary_ids, $user_items)) > 0) $badge_type = 'Primary';
                if ( count(array_intersect($junior_ids, $user_items)) > 0) $badge_type = 'Junior';
                if ( count(array_intersect($earliteen_ids, $user_items)) > 0) $badge_type = 'Earliteen';
                if ( count(array_intersect($youth_ids, $user_items)) > 0) $badge_type = 'Youth';

                // check if discount are used 15MealsOnly7w9
                //$thecode = '15MealsOnly7w9'; 
                $thecode = '16MealsOnly2yb'; 
                $sql2 = " SELECT * from civicrm_participant pt 
                    where pt.fee_level like '%{$thecode}%' and pt.id={$one_row['id']} 
                ";
                $dao2 = CRM_Core_DAO::executeQuery($sql2);
                if ( $dao2->fetch() ) {
                    $badge_type = 'Meals Only';
                }


                if ( count(array_intersect($singleday_ids, $user_items)) > 0) $badge_type = 'Single Day';



     			
     			

     		}

            // check does it bye breakfast
            // UNCOMMENT TO HAVE INVITATION
            // $breakfast_res = array_intersect ( $breakfast_items, $user_items );
            // if (!empty($breakfast_res)) {
            //     $one_row['buy_breakfast'] = true;
            // }


     		$one_row['badge_type'] = $badge_type;

     		// $one_row['fta'] = 1;


     		// encode particiapnt id to hashids
      		// var_dump($one_row['id']);
            
      		$one_row['participant_id'] = $one_row['id'];
            $one_row['id'] = $hashids->encode($one_row['id']);
      		// var_dump($one_row['id']);

     		$all[] = $one_row;
    	}

    // var_dump($this);
        $this->assign('data',$all);

        $res = CRM_Core_Resources::singleton();
        // $res->addScriptFile('com.netasi.badge', 'templates/CRM/js/my0.js',99);
        $res->addScriptFile('com.netasi.badge', 'templates/CRM/js/CODE128.JsBarcode.min.js',101);
        $res->addScriptFile('com.netasi.badge', 'templates/CRM/js/my.js',102);
        $res->addStyleFile('com.netasi.badge', 'templates/CRM/css/my.css',10);
    }


    function preProcess() {
    // parent::preprocess();

    // // set print view, so that print templates are called
    $this->controller->setPrint(1);
    // $controller->setEmbedded(TRUE);
    // $controller->run();

    // get the formatted params
	}

	public function postProcess(){
		error_log($this->_contactIds);
	}

}
