<?php

/**
 * Collection of upgrade steps
 */
class CRM_Badge_Upgrader extends CRM_Badge_Upgrader_Base {

  // By convention, functions that look like "function upgrade_NNNN()" are
  // upgrade tasks. They are executed in order (like Drupal's hook_update_N).

  /**
   * Example: Run an external SQL script when the module is installed
   *
  public function install() {
    $this->executeSqlFile('sql/myinstall.sql');
  }

  /**
   * Example: Run an external SQL script when the module is uninstalled
   *
  public function uninstall() {
   $this->executeSqlFile('sql/myuninstall.sql');
  }

  /**
   * Example: Run a simple query when a module is enabled
   */

  public function enable() {
    $param = '{"name":"dymo","paper-size":"dymo","metric":"mm","lMargin":15,"tMargin":26,"NX":1,"NY":1,"SpaceX":10,"SpaceY":5,"width":83,"height":57,"font-size":12,"orientation":"landscape","font-name":"helvetica","font-style":"","lPadding":3,"tPadding":3}';

    $str = "INSERT INTO  civicrm_option_value (
`option_group_id` ,
`label` ,
`value` ,
`name` ,
`grouping` ,
`filter` ,
`is_default` ,
`weight` ,
`description` ,
`is_optgroup` ,
`is_reserved` ,
`is_active` ,
`component_id` ,
`domain_id` ,
`visibility_id`
)
VALUES (
 '74',  'dymo', '$param', 'dymo', NULL ,  '0', NULL ,  '1', NULL ,  '0',  '0',  '1', NULL , NULL , NULL
)";
    CRM_Core_DAO::executeQuery($str);
    $param = '{"metric":"pt","width":175.5,"height":301.5}';
    $str = "INSERT INTO  civicrm_option_value (
`option_group_id` ,
`label` ,
`value` ,
`name` ,
`grouping` ,
`filter` ,
`is_default` ,
`weight` ,
`description` ,
`is_optgroup` ,
`is_reserved` ,
`is_active` ,
`component_id` ,
`domain_id` ,
`visibility_id`
)
VALUES (
 '58',  'dymo',  '$param',  'dymo', NULL , NULL ,  '0',  '24', NULL ,  '0',  '0',  '1', NULL , NULL , NULL
)";
    CRM_Core_DAO::executeQuery($str);
  

  }

  /**
   * Example: Run a simple query when a module is disabled
   *
   */
  public function disable() {
    CRM_Core_DAO::executeQuery("DELETE from civicrm_option_value where label='dymo' " );
  }

  /**
   * Example: Run a couple simple queries
   *
   * @return TRUE on success
   * @throws Exception
   *
  public function upgrade_4200() {
    $this->ctx->log->info('Applying update 4200');
    CRM_Core_DAO::executeQuery('UPDATE foo SET bar = "whiz"');
    CRM_Core_DAO::executeQuery('DELETE FROM bang WHERE willy = wonka(2)');
    return TRUE;
  } // */


  /**
   * Example: Run an external SQL script
   *
   * @return TRUE on success
   * @throws Exception
  public function upgrade_4201() {
    $this->ctx->log->info('Applying update 4201');
    // this path is relative to the extension base dir
    $this->executeSqlFile('sql/upgrade_4201.sql');
    return TRUE;
  } // */


  /**
   * Example: Run a slow upgrade process by breaking it up into smaller chunk
   *
   * @return TRUE on success
   * @throws Exception
  public function upgrade_4202() {
    $this->ctx->log->info('Planning update 4202'); // PEAR Log interface

    $this->addTask(ts('Process first step'), 'processPart1', $arg1, $arg2);
    $this->addTask(ts('Process second step'), 'processPart2', $arg3, $arg4);
    $this->addTask(ts('Process second step'), 'processPart3', $arg5);
    return TRUE;
  }
  public function processPart1($arg1, $arg2) { sleep(10); return TRUE; }
  public function processPart2($arg3, $arg4) { sleep(10); return TRUE; }
  public function processPart3($arg5) { sleep(10); return TRUE; }
  // */


  /**
   * Example: Run an upgrade with a query that touches many (potentially
   * millions) of records by breaking it up into smaller chunks.
   *
   * @return TRUE on success
   * @throws Exception
  public function upgrade_4203() {
    $this->ctx->log->info('Planning update 4203'); // PEAR Log interface

    $minId = CRM_Core_DAO::singleValueQuery('SELECT coalesce(min(id),0) FROM civicrm_contribution');
    $maxId = CRM_Core_DAO::singleValueQuery('SELECT coalesce(max(id),0) FROM civicrm_contribution');
    for ($startId = $minId; $startId <= $maxId; $startId += self::BATCH_SIZE) {
      $endId = $startId + self::BATCH_SIZE - 1;
      $title = ts('Upgrade Batch (%1 => %2)', array(
        1 => $startId,
        2 => $endId,
      ));
      $sql = '
        UPDATE civicrm_contribution SET foobar = whiz(wonky()+wanker)
        WHERE id BETWEEN %1 and %2
      ';
      $params = array(
        1 => array($startId, 'Integer'),
        2 => array($endId, 'Integer'),
      );
      $this->addTask($title, 'executeSql', $sql, $params);
    }
    return TRUE;
  } // */

}
