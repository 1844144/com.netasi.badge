



$(function ()  {


	function select_by_text(elem, text){
		elem.find('option').filter(function() {
		    return this.text == text; 
		}).attr('selected', true);

	}

	$('.badge').on('click', ".can-edit:not(.edit-now)", function(e) {
		var main_el = $(this);
		var cur_val = $(this).text();
		main_el.append(" <div id='all'> <input id='input'> <button id='ok'>Ok</button> <button id='cancel' > Cancel</button> </div>");
		main_el.addClass('edit-now');
		main_el.find('#input').val(cur_val);
	});

	function fill_country ( my_country) {
		var template = _.template(" \
							  		  	<% _.each ( result.values, function (obj) { %> \
							  		  		<option value=<%= obj.key %>><%= obj.value %></option> \
							  		    <% }); %> 		  \
							  ");
		CRM.api3('Address', 'getoptions', {
					  "sequential": 1,
					  "field": "country_id",
				}).done(function(result3){
					var options = template( {'result': result3});
					$('.city #country').html(options);
					if ( my_country != undefined ) {
						$('.city #country').val( my_country );
					}
					else {
						$('.city #country').val( "1228").change();
					}

				});

	}

	$('.badge').on('click', ".city:not(.edit-now)", function(e) {
		var main = $(this).closest('.badge');
		var contact_id = main.attr('orig-id');
		var main_el = $(this);
		main_el.append(" <div id='all'> <input id='input'> <select id='select'> </select> <select id='country'> </select> <button id='ok'>Ok</button> <button id='cancel' > Cancel</button> </div>");
		main_el.addClass('edit-now');

		// init elements 
		main_el.find('#input').val( main_el.find('.scity').text() );

		var country = main_el.find('.scountry').text();
		if (country == '') country = 'United States';
				main_el.find('#country').one('change', function(){ 
					
					var state = main_el.find('.sstate').text();
					main_el.find('#select').one('change', function(){
						select_by_text( main_el.find('#select') , state);
					});
					
					select_by_text( main_el.find('#country') , country);
				});
				fill_country();

		// CRM.api3('Address', 'get', {
		//   "sequential": 1,
		//   "contact_id": contact_id,
		//   "is_primary": 1,
		// }).done(function(result){
		// 	if (result.values.length != 0) {
		// 		// e.g. person have address

		// 			main_el.find('#select').one('change', function(){ 
		// 				$('.city #select').val( result.values[0].state_province_id);
		// 			});
		// 			fill_country( result.values[0].country_id);
		// 	}
		// 	else {
				
		// 	}
		// });
		


	});

	$('.city').on('change', "#country", function() {
		CRM.api3('Address', 'getoptions', {
			  "sequential": 1,
			  "field": "state_province_id",
			  "country_id": $(this).val(),  
			}).done(function(result) {
				var template = _.template(" \
							  		  	<% _.each ( result.values, function (obj) { %> \
							  		  		<option value=<%= obj.key %>><%= obj.value %></option> \
							  		    <% }); %> 		  \
							  ");
				var options = template( {'result': result});
				$('.city #select').html(options).change();
		});
	});

	$('.city').on('click', "#ok", function() {
		// save to database
		var id = $(this).closest('.badge').attr('orig-id');

		var main = $(this).closest('.badge');

		var new_city = $('.city #input').val();
		var state_id = $('.city #select').val();
		var state_text = $('.city #select :selected').text();

		var country_id = $('.city #country').val();
		var country_text = $('.city #country :selected').text();
		// show in browser
		main.find('.city .scity').text(new_city);
		main.find('.city .sstate').text(state_text);
		main.find('.city .scountry').text(country_text);

		// save in database 
		// city
		// if it has address

		CRM.api3('Address', 'get', {
		  "sequential": 1,
		  "contact_id": id,
		  "is_primary": 1,
		}).done(function(result){
			if (result.values.length != 0) {
				// update
				CRM.api3('Address', 'setvalue', {
					  "sequential": 1,
					  "field": "city",
					  "id": result.values[0].id,
					  "value": new_city,
				});
				CRM.api3('Address', 'setvalue', {
					  "sequential": 1,
					  "field": "state_province_id",
					  "id": result.values[0].id,
					  "value": state_id,
				});	
				CRM.api3('Address', 'setvalue', {
					  "sequential": 1,
					  "field": "country_id",
					  "id": result.values[0].id,
					  "value": country_id,
				});				


			}
			else {
				// create new
				CRM.api3('Address', 'create', {
				  "sequential": 1,
				  "contact_id": id,
				  "location_type_id": "Home",
				  "is_primary": 1,
				  state_province_id: state_id,
				  city: new_city,
				  country_id: country_id
				});
			}

		});
		

		
		// remove all
		$('.city #all').remove();
		$('.city').removeClass('edit-now');

	}); 

	$('.city').on('click', "#cancel", function() {
		// save to database
		// remove all
		$('.city #all').remove();
		$('.city').removeClass('edit-now');
	});



	$('.can-edit').on('click', "#ok", function() {
		// save to database
		var id = $(this).closest('.badge').attr('participant-id');
		var main = $(this).closest('.badge');

		CRM.api3('Participant', 'create', {
			  "sequential": 1,
			  "id": id,
			  "custom_49": $('.can-edit #input').val()
		});
		main.find('.can-edit').text($('.can-edit #input').val());
		// remove all
			$('.can-edit #all').remove();
			$('.can-edit').removeClass('edit-now');
	}); 

	$('.can-edit').on('click', "#cancel", function() {
		// save to database
		// remove all
		$('.can-edit #all').remove();
		$('.can-edit').removeClass('edit-now');
	});

	// --------------------------------
	// for fta
	// --------------------------------

	$('.badge').on('click', ".ask_fta:not(.edit-now)", function(e) {
		var main_el = $(this);
		// var cur_val = $(this).text();
		main_el.append(" <div id='all'> <label><input type='radio' name='ask-fta' value='1'>Yes</input></label> <label><input type='radio' name='ask-fta' value='2'>No</input></label> <button id='ok'>Ok</button> <button id='cancel' > Cancel</button> </div>");
		main_el.addClass('edit-now');
		var main = $(this).closest('.badge');
		main.find('.participant_status img:hidden, .wifi-block .title:hidden').addClass('fta-trigger');
		// main_el.find('#input').val(cur_val);
	});
	$('.ask_fta').on('click', "#cancel", function() {
		// save to database
		// remove all
		$('.ask_fta #all').remove();
		$('.ask_fta').removeClass('edit-now');
	});

	$('.ask_fta').on('click', "#ok", function() {
		// save to database
		var id = $(this).closest('.badge').attr('participant-id');
		var main = $(this).closest('.badge');

		var val = $('.ask_fta input:checked').val()

		if (val == "1") {
			main.find('.fta-trigger').show();
		}
		else if (val=="2") {
			main.find('.fta-trigger').hide();
		}
		main.find('.fta-trigger').removeClass('fta-trigger');
		CRM.api3('Participant', 'create', {
			  "sequential": 1,
			  "id": id,
			  "custom_48": val
		});
		main.find('.ask_fta').text('');
		// remove all
			$('.ask_fta #all').remove();
			$('.ask_fta').removeClass('edit-now');
			main.find('.ask_fta').remove();

	}); 
	// -----------------------
	// for first name and last name
	// ------------------------
	$('.badge').on('click', ".first_last_name:not(.edit-now)", function(e) {
		var main_el = $(this);
		var main = $(this).closest('.badge');
		var cur_first = main.find('#firstname').text();
		var cur_last = main.find('#lastname').text();
		main_el.append(" <div id='all'> <input id='input' name='firstname'> <input id='input' name='lastname'> <button id='ok'>Ok</button> <button id='cancel' > Cancel</button> </div>");
		main_el.addClass('edit-now');

		main.find('input[name=firstname]').val(cur_first);
		main.find('input[name=lastname]').val(cur_last);
	});

	$('.first_last_name').on('click', "#ok", function() {
		// save to database
		var id = $(this).closest('.badge').attr('orig-id');
		var main = $(this).closest('.badge');

		var cur_first = main.find('input[name=firstname]').val();
		var cur_last = main.find('input[name=lastname]').val();

		main.find('#firstname').text(cur_first);
		main.find('#lastname').text(cur_last);

		CRM.api3('Contact', 'create', {
			  "sequential": 1,
			  "id": id,
			  "first_name": cur_first,
			  "last_name": cur_last,
		});
		// remove all
			$('.first_last_name #all').remove();
			$('.first_last_name').removeClass('edit-now');
	}); 

	$('.first_last_name').on('click', "#cancel", function() {
		// save to database
		// remove all
		$('.first_last_name #all').remove();
		$('.first_last_name').removeClass('edit-now');
	});

	





})