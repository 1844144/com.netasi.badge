{* 
	You can use thess fields
	
	p.first_name
	p.last_name
	p.city
	p.state
	p.country
	p.company
	p.prefix
	p.suffix
	p.id - participant id, used for qr

	p.display_name - Display name of Registrar user

*}
<div class="badge {if $p.badge_type=='Meals Only'} meals-only {elseif $p.badge_type=='Single Day'} single-day  {elseif isset($p.is_exibitor_man)}exhibitor{/if}" orig-id={$p.orig_id} participant-id={$p.participant_id} >
	<div class="vspace"></div>
	{if $p.status_id==2}<div style='position:relative'><div class="attended">DUPLICATE</div></div>{/if}
	{if $p.badge_type=='Meals Only'}<div style='position:relative'><div class="not-registered">NOT REGISTERED</div></div>{/if}
	<!-- if will change again: проблемы с сокрытием приглашения и значака если изменяешь несколько рарз -->
	{if $p.fta != 1 and $p.fta != 2} <div style='position:relative'><div class="ask_fta ">First Time Attending: Yes/No ?</div></div> {/if}
	
	<div class="half" id='h1'>
		<div style="position:relative">
			<div style="position:absolute">
				
		{if $p.company_changed} <div class="warning">Organization Name from {$p.display_name}</div>{/if}
		{if $p.city_changed} <div class="warning">City from {$p.display_name}</div>{/if}
		{if $p.state_changed} <div class="warning">State from {$p.display_name}</div>{/if}
		{if $p.country_changed} <div class="warning">Country from {$p.display_name}</div>{/if}
			</div>
		</div>
		
		{if isset($p.meals) }
		<div class="meal-title"> Meals Purchased </div>
		<div class="meals"> 
			{foreach from=$p.meals item=m}
				{$m} <br> 
			{/foreach}
			{if isset($p.wifi)}
			<span class='wifipwd'>Wifi: ASI16W1T2F3S4</span> <br>
		    {/if}
		</div>
		{/if}
		<div class="wifi-block">
			{if  isset($p.buy_breakfast) }
			<div class="title" {if ( isset($p.is_new) or $p.fta==1  or $p.badge_type=='ASI Board Member' ) }  style='display:block' {/if}>
				<div class="invite">
					<div class="title1">Invitation</div>
					<div class="text1"> New Member & First-Time Attendee Breakfast <br>
		7:45 a.m. in Room 300 B-D </div>
				</div>
			</div>
			{/if}
		
		
		</div>

		
	</div>
	<div class="half" id='h2'>
		<div class='mybox'> 
			<div class="center1">
				
				<div class="name first_last_name"><div id='fname'>{$p.prefix} <span id='firstname'>{$p.first_name}</span></div> <div id='lname'><span id='lastname'>{$p.last_name}</span> {$p.suffix}</div></div>
				<div style='width: 10px'></div>

				<div class="ministry can-edit">{$p.company}</div>
				<div class="city can-edit2"><span class='scity'>{$p.city}</span>{if $p.state}, {/if}<span class='sstate'>{$p.state}</span>{if isset($p.country) and ($p.country!="United States")}, <span class='scountry'>{$p.country}</span>{/if}</div>

				</div>

				<div class="ff2">
			</div>
		</div>
	<div class="asi-and-qr">
		<!-- <img src="http://files.asiministries.org/logo/asi-logo-600px.png" class='asi-logo'> -->
		<img class='make-qr' code='{$p.id}' width='200px' height="30px"></img>

	</div>
	<div class="participant_status"><img src="http://files.asiministries.org/convention/FTA-badge.png" id='img_fta' {if $p.fta==1 } style='display:block'{/if} > {$p.badge_type}</div>
	</div>
</div>
