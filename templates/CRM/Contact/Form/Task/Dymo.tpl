{literal}
<style>

	
	.badge {
		width: 7.25in;
		height: 5.5in;
		border: 1px solid gray; 
	}
	.half {
		/* ADJUST DISTANCE TO TOP OF PAGE HERE*/
		padding-top: 130pt;
		height: 238pt;
		width: 45%;
		float: left;
		min-height: 5cm;
		position: relative;
	}
	.asi-and-qr{
		position:  absolute;
		top: 262pt;
		width: 87%;
		margin: 0 24px;
	}
	.asi-logo {
		width: 20pt;
		float: left;
		width: 100pt;
		margin-top: 6px;
	}

	.meal-title{
		margin-top: 30pt;
		font-family: GothamCondensedMedium;
		font-size: 24pt;
		color: black;
		text-align: center;
		display: none;
	}
	.meals {
    font-family: GothamNarrowBook;
    color: black;
    line-height: 14pt;
     -webkit-column-count: 2;
    -moz-column-count: 2;
         column-count: 2;
    text-align: left;
    position: absolute;

    font-size: 12px; 
    /* TO ALIGN MEALS BLOCK*/
    top: 410px;
    left: 21px;
    margin-top: 6mm;
    margin-left: -6mm;

	}
	.wifi-block .title {
		font-family: GothamBlack;
		font-size: 14pt;
		/*color: #263872;*/
		position: absolute;
		width: 100%;
	}
	.wifi-block .pwd {
		position: absolute;
		top: 60px;
		width: 100%;
	}
	.wifi-block b, .wifipwd {
		font-family: GothamNarrowBold;
		font-size: 8pt;
	}
	.wifi-block {
		text-align: center;
		font-family: GothamNarrowBold;
		font-size: 8pt;
		position: absolute;
		text-align: center;
		width: 100%;
		top: 310pt;
	}
	.warning {
		font-style: bold;
		color: red;
	}
	.attended {
		color: red;
		font-size: 30pt;
position: absolute;
top: 176px;
right: 129px;
z-index: 100;
	}
	.ask_fta {
		color: red;
		font-size: 16pt;
		position: absolute;
		top: 136px;
		right: 116px;
		z-index: 100;

	}
	#img_fta{
		position: absolute;
width: 75px;
left: -14px;
top: -25px;
	}


	.name {
		margin-top: 41pt;
		font-size: 30pt;
		line-height: 26pt;
		text-align: center;
		font-family: GothamCondensedMedium;

	}
	.wifi-block .title {
		display: none;
	}
	.invite .title1{
		font-family: GothamCondensedMedium;
		font-size: 17pt;
		line-height: 17pt;
		color: #263872;
	}
	.invite .text1{
		font-family: GothamNarrowBook;
		font-size: 10pt;
line-height: 13pt;
margin-top: 3px;

	}
	.not-registered {
		color: red;
font-size: 26pt;
position: absolute;
top: 176px;
right: 75px;
z-index: 100;

	}
	.participant_status {
		position: absolute;
		bottom: 10mm;
		display: block;
		width: 88%;
		margin: 0 7%;
		text-align: center;
		background: #263872;
		font-family: GothamNarrowBlack;
		font-size: 12pt;
		color: white;
		line-height: 27pt;
	}
	.participant_status img {
		display: none;
	}
	.exhibitor .participant_status{
		background: #f05557;
	}
	.single-day .participant_status{
		background: #69a9d2;
	}
	.meals-only .participant_status{
		background: #eab887;
	}
	/* HALF 1*/
	.half#h1{
		margin-left: 3px; /* zero, e.g. without changes, -10px to move left, 10px to move rigth*/

	}
	/* HALF 2*/
	.half#h2{
		margin-left: 0px;
		text-align: center;
		margin-top: 10mm;
		/*top: 37px;*/
	}
	.half {
		left: 82px;
	}

	.ministry{
		margin-top: 5pt;
		text-align: center;
/*		min-height: 42pt;*/
		font-size: 14pt;
		font-family: GothamNarrowBookItalic;
		line-height: 15pt;
		color: #263872 ;
	}
	.city {
		text-align: center;
		margin-bottom: 7pt;
		font-family: GothamNarrowBook;
		font-size: 11pt;
		line-height: 17pt;
		color: black;


	}
	.ff1,.ff2 {
		display: inline-block;
		float: left;
		text-align: left;
	}
	.ff2 {
		text-align: right;
		float: right;
		padding-right: 40pt;
	}
	.ff1 {
		line-height: 16pt;
		background: black;
		color: white;
		padding: 10px 20px;
		margin-top: 44px;
		margin-left: 19px;
		width: 150px;
		font-size: 20px;
		font-weight: bold;
		font-family: Arial;
	}
	.make-qr {
		display: inline-block;
		margin-top: 4pt;
	}
	.badge:hover .can-edit::after, .badge:hover .can-edit2::after, .badge:hover .ask_fta::after, .badge:hover .first_last_name::after {
		content: " [Click to edit]";
		cursor: pointer;
		z-index: 10;
		position: relative;	
		background: rgba(255,255,255,0.8);
	}

	.badge:hover .city, .badge:hover .ministry {
		z-index: 10;
		position: relative;	
		background: rgba(255,255,255,0.9);	
	}

	

	.badge:hover .edit-now::after  {
		content: ""
	}
	.edit-now {
		position: relative;
		z-index: 10;
	}

	.edit-now.ask_fta {
		position: absolute;

	}
	.ask_fta label:hover{
		cursor: pointer;
		background: #eee;

	}
	/* style of First Name*/
	#fname {
	font-size: 50pt;
	line-height: 0pt;

	} 
	/* style of Last Name*/
	#lname {
		font-size: 30pt;
		/* padding-top: 10px;*/
		line-height: 20pt;
		margin-top: 34px;
	}

	/* HERE YOU CAN SEE BOX*/
	.mybox {
		display: table;
		/*uncomment to see edges*/
		/*border: 1px solid black;*/
		height: 165px;
		width: 100%;
	}

	/* and text borders */
	.center1 {
		/*uncomment to see edges*/
		/*border: 1px solid blue;*/
		display: inline-block;
		vertical-align: middle;


	}

	.mybox::before {
		height: 100%;
		content: "";
		display: inline-block;
		vertical-align: middle;
	}

	div#all {
			background: rgba(200,200,200,0.8);
		padding: 10px;
		border-radius: 5px;
		text-align: left;
	}

	





	@media print {
		/* remove extra 5pt on top */ 
		body { margin: 0px; }
		.badge {
			page-break-before: always;
			border: none;

		}
		.badge:first-child {
			page-break-before: avoid;
			border: none;
		}
		.dont-print,.warning {
			display: none;
		}
	}
</style>
{/literal}

<div>
{foreach from=$data item=item }
	{include file="CRM/Contact/Form/Task/DymoLabel.tpl" p=$item}
{/foreach}
</div>
{literal}
<!-- make actual qr-codes -->

<script type="text/javascript">
	// window.print();

</script>
{/literal}
